package pe.ideas.creativas.merakiventas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import pe.ideas.creativas.merakiventas.config.StarterApplication;

@SpringBootApplication
public class MerakiVentasApplication extends StarterApplication{

	public static void main(String[] args) {
		SpringApplication.run(MerakiVentasApplication.class, args);
	}

}
