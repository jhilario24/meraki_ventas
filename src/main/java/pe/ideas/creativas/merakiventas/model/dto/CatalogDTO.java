package pe.ideas.creativas.merakiventas.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CatalogDTO {
	private String catalogId;
	private String catalogDescription;
}
