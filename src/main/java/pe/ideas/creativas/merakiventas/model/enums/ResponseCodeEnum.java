package pe.ideas.creativas.merakiventas.model.enums;

import java.util.stream.Stream;

import org.hibernate.PropertyNotFoundException;

import lombok.Getter;
import lombok.NonNull;

@Getter
public enum ResponseCodeEnum {
	  OK ("PC0001", "PROCESO CONFORME"),
	  PROCESSING_ERROR ("PE0001", "ERROR AL PROCESAR TRANSACCION");
	
	  private String resultCode;
	  private String resultDescription;
	  
	  ResponseCodeEnum(String resulCode, String resultDescription) {
		  this.resultCode=resulCode;
		  this.resultDescription=resultDescription;
	  }
	  
	  
	  public static ResponseCodeEnum fromCode(@NonNull String input) {
		    return Stream.of(ResponseCodeEnum.values())
		        .filter(code -> code.resultCode.equalsIgnoreCase(input))
		        .findFirst()
		        .orElseThrow(() -> new PropertyNotFoundException(input));
		  }
	
}
