package pe.ideas.creativas.merakiventas.config;

import java.net.InetAddress;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.core.env.Environment;

import lombok.extern.slf4j.Slf4j;
import reactor.util.Loggers;
@Slf4j
public class StarterApplication implements CommandLineRunner{
	  @Autowired
	  private ApplicationContext appContext;
	  
	  
	  @Override
	  public void run(String... args) throws Exception {
	    Loggers.useSl4jLoggers();
	    Environment env = appContext.getEnvironment();
	    
	    String protocol = "http";
	    if ("true".equals(env.getProperty("server.ssl.enabled"))) {
	      protocol = "https";
	    }
	    
	    log.info(
	            "\n--------------------------------------------------------\n\t"
	                + "Application '{}' is running! Access URLs:\n\t" + "Local: \t\t{}://localhost:{}\n\t"
	                + "External: \t{}://{}:{}\n\t"
	                + "Root Path: \t{}\n\t"
	                + "Profile(s): \t{}\n--------------------------------------------------------",
	            env.getProperty("spring.application.name"), protocol, env.getProperty("server.port"), protocol,
	            InetAddress.getLocalHost().getHostAddress(), env.getProperty("server.port"),
	            Optional.ofNullable(env.getProperty("application.context-path"))
	              .orElse("<Not configured>"),
	            env.getActiveProfiles());
}
}


//package pe.alicorp.altemista.runner;
//
//import java.net.InetAddress;
//import java.util.Optional;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.CommandLineRunner;
//import org.springframework.context.ApplicationContext;
//import org.springframework.core.env.Environment;
//import reactor.blockhound.BlockHound;
//import reactor.netty.resources.NettyBlockHoundIntegration;
//import reactor.util.Loggers;
//
//@Slf4j
//public class StarterWebApplication implements CommandLineRunner {
//
//  @Autowired
//  private ApplicationContext appContext;
//
//  @Override
//  public void run(String... args) throws Exception {
////    BlockHound.builder()
////        .allowBlockingCallsInside("java.io.FileInputStream", "read")
////        .allowBlockingCallsInside("java.io.FileInputStream", "readBytes")
////        .with(new NettyBlockHoundIntegration())
////        .install();
//    Loggers.useSl4jLoggers();
//    Environment env = appContext.getEnvironment();
//    String protocol = "http";
//    if ("true".equals(env.getProperty("server.ssl.enabled"))) {
//      protocol = "https";
//    }
//    log.info(
//        "\n--------------------------------------------------------\n\t"
//            + "Application '{}' is running! Access URLs:\n\t" + "Local: \t\t{}://localhost:{}\n\t"
//            + "External: \t{}://{}:{}\n\t"
//            + "Root Path: \t{}\n\t"
//            + "Profile(s): \t{}\n--------------------------------------------------------",
//        env.getProperty("spring.application.name"), protocol, env.getProperty("server.port"), protocol,
//        InetAddress.getLocalHost().getHostAddress(), env.getProperty("server.port"),
//        Optional.ofNullable(env.getProperty("application.context-path"))
//          .orElse("<Not configured>"),
//        env.getActiveProfiles());
//  }


