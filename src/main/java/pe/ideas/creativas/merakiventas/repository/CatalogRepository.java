package pe.ideas.creativas.merakiventas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import pe.ideas.creativas.merakiventas.model.entity.Catalog;

@Repository
public interface CatalogRepository extends JpaRepository<Catalog,String> {
	
	Catalog findByCatalogDescription(String catalogDescription);
//	Catalog save(CatalogDTO request);
//	CatalogDTO savCatalogDTO

}
