package pe.ideas.creativas.merakiventas.expose.web;

import java.util.List;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import com.github.javafaker.Faker;
import lombok.extern.slf4j.Slf4j;
import pe.ideas.creativas.merakiventas.model.dto.CatalogDTO;
import pe.ideas.creativas.merakiventas.model.entity.Catalog;
import pe.ideas.creativas.merakiventas.model.enums.ResponseCodeEnum;
import pe.ideas.creativas.merakiventas.response.CatalogResponse;
import pe.ideas.creativas.merakiventas.service.CatalogoService;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("${application.context-path}")
@Slf4j
public class MerakiVentasController {

	CatalogoService catalogoService;
	private static final Faker faker = Faker.instance();
	ResponseCodeEnum responseCode;
	
	
	
	public MerakiVentasController(CatalogoService catalogoService) {
		this.catalogoService = catalogoService;
	}
	
	@ResponseStatus(value = HttpStatus.ACCEPTED)
	@GetMapping(value = "/getCatalog", produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.APPLICATION_STREAM_JSON_VALUE })
	public Mono<List<Catalog>> getCatalog() {

		return catalogoService.getCatalogs();
	}
	
	
	

	
	@ResponseStatus(value = HttpStatus.CREATED)
	@PostMapping(value = "/saveCatalog", produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.APPLICATION_STREAM_JSON_VALUE })
	public Mono<CatalogResponse> saveCatalog(@RequestBody CatalogDTO request)  {
		responseCode = ResponseCodeEnum.fromCode("PC0001"); // --> codigo en duracel de proceso
		CatalogResponse response = new CatalogResponse();
		catalogoService.saveCatalog(request);
		response.setCodeResponse(responseCode.getResultCode());
		response.setDescriptionResponse(responseCode.getResultDescription());
		return Mono.just(response)
				.doOnError(onError -> {
					responseCode = ResponseCodeEnum.fromCode("PE0001");
					response.setCodeResponse(responseCode.getResultCode());
					response.setDescriptionResponse(responseCode.getResultDescription());
				});
	}
//
	
	@PutMapping(value = "/{idCatalog}/getCatalog")
	public Catalog updateCatalogByID(@PathVariable String idCatalog, @RequestBody CatalogDTO request) {

		return catalogoService.updateCatalog(idCatalog, request);

	}
//
	@DeleteMapping("/{idCatalog}/deleteCatalog")
	public void deleteCatalog(@PathVariable String idCatalog) {
		catalogoService.removeCatalog(idCatalog);
	}

//	@RestController
//	@RequestMapping("users")
//	public class UserController {
//	    @Autowired
//	    UserService userService;
//	 
//	    @PutMapping("/{userId}", 
//	    consumes={MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE}, 
//	    produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE} )
//	    public UserRest updateUser(@PathVariable String userId, @RequestBody UserDetailsRequestModel requestUserDetails)    {
//	        UserRest returnValue = new UserRest();
//	        UserDto userDto = new UserDto();
//	        BeanUtils.copyProperties(requestUserDetails, userDto);
//	        UserDto updatedUserDetails = userService.updateUser(userDto, userId);
//	        BeanUtils.copyProperties(updatedUserDetails, returnValue);
//	        return returnValue;
//	    }
//	}

}
