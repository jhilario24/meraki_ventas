package pe.ideas.creativas.merakiventas.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CatalogResponse {
	private String codeResponse;
	private String descriptionResponse;
}
