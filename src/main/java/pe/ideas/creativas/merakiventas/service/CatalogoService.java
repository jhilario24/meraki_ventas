package pe.ideas.creativas.merakiventas.service;

import java.util.List;

import pe.ideas.creativas.merakiventas.model.dto.CatalogDTO;
import pe.ideas.creativas.merakiventas.model.entity.Catalog;
import reactor.core.publisher.Mono;

public interface CatalogoService {
	Mono<List<Catalog>> getCatalogs();
	Mono<Catalog> saveCatalog(CatalogDTO request);
	Catalog updateCatalog(String idCatalog, CatalogDTO request);
	void removeCatalog(String idCatalog);

}
