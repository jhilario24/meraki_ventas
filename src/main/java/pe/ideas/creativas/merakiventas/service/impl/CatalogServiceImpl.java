package pe.ideas.creativas.merakiventas.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import pe.ideas.creativas.merakiventas.exceptions.UserExistException;
import pe.ideas.creativas.merakiventas.model.dto.CatalogDTO;
import pe.ideas.creativas.merakiventas.model.entity.Catalog;
import pe.ideas.creativas.merakiventas.repository.CatalogRepository;
import pe.ideas.creativas.merakiventas.service.CatalogoService;
import pe.ideas.creativas.merakiventas.utils.ConverMapperUtils;
import reactor.core.publisher.Mono;

@Service
@Slf4j
public class CatalogServiceImpl implements CatalogoService {
	@Autowired
	CatalogRepository catalogRepository;
	
	public CatalogServiceImpl(CatalogRepository catalogRepository) {
		this.catalogRepository=catalogRepository;
	}

	@Override
	public Mono<List<Catalog>> getCatalogs() {
		
		return  Mono.just(catalogRepository.findAll());
	}

	@Override
	public Mono<Catalog> saveCatalog(CatalogDTO request) {	
		

		Catalog entityRequest =ConverMapperUtils.convertToEntity(request);
		Catalog existing = catalogRepository.findByCatalogDescription(entityRequest.getCatalogDescription());
		
		if(existing != null) {
			throw new  UserExistException(existing.getCatalogDescription(),"El catalogo :"+existing.getCatalogDescription()+"  a insertar, ya existe");
		}
		
		
		return Mono.just(catalogRepository.save(entityRequest));
	}

	@Override
	public Catalog updateCatalog(String idCatalog, CatalogDTO request) {
		return catalogRepository.findById(idCatalog)
				.map(objeto -> {
					objeto.setCatalogDescription(request.getCatalogDescription());
					return catalogRepository.save(objeto);
				})
				.orElseGet(() ->{
					request.setCatalogId(idCatalog);
					return catalogRepository.save(ConverMapperUtils.convertToEntity(request));
					
				});
				
	}

	@Override
	public void removeCatalog(String idCatalog) {
		catalogRepository.deleteById(idCatalog);
	}
	

}
