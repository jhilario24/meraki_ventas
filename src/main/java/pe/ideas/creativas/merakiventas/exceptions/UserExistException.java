package pe.ideas.creativas.merakiventas.exceptions;

//@ResponseStatus(value = HttpStatus.BAD_REQUEST,reason="user has exist!")
public class UserExistException extends RuntimeException
{
    /**
	 * 
	 */
	private final String valor;
	public UserExistException(String valor,String errorMsg)
    {
		
        super(errorMsg);
        this.valor=valor;
    }
}