package pe.ideas.creativas.merakiventas.utils;

import java.util.UUID;

import pe.ideas.creativas.merakiventas.model.dto.CatalogDTO;
import pe.ideas.creativas.merakiventas.model.entity.Catalog;

public final class ConverMapperUtils {
	
	public static  CatalogDTO convertToDTO(Catalog entity) {
		CatalogDTO dto = new CatalogDTO();
		dto.setCatalogId(entity.getCatalogId());
		dto.setCatalogDescription(entity.getCatalogDescription());
		return dto;
	}
	
	public static  Catalog convertToEntity(CatalogDTO dto) {
		Catalog entity = new Catalog();
		entity.setCatalogId(UUID.randomUUID().toString());
		entity.setCatalogDescription(dto.getCatalogDescription());
		return entity;
	}

}
